import numpy
from paraview.simple import *
from paraview import vtk

# *****************
# Pipeline
# *****************

# -----------------------------------------------------------------------------
def __CreatePipelineSource(grid, name='MyGrid', representation='Surface'):
    """ Create a data producer containing the input vtk grid as data.
    Add it to the pipeline and show it. By default, use the Surface representation.
    Works only on builtin server.
    """
    producer = PVTrivialProducer(guiName=name)
    cso = producer.GetClientSideObject()
    cso.SetOutput(grid)
    rep = Show(producer)
    rep.Representation = representation
    Render()
    return producer

# -----------------------------------------------------------------------------
def CreateRegularGrid(extent, name, origin=[0, 0, 0], spacing=[1, 1, 1]):
    """Create a pipeline source with a vtkImageData.
    Works only on builtin server.
    """
    grid = vtk.vtkImageData()
    grid.SetExtent(extent)
    grid.SetOrigin(origin)
    grid.SetSpacing(spacing)
    return __CreatePipelineSource(grid, name)

# -----------------------------------------------------------------------------
def CreateRegularGridFromData(numpyData, name="MyGrid", dataName="MyDataArray"):
    """Create a producer with a vtkImageData.
    Works only on builtin server.
    """
    s = numpyData.shape
    if len(s) != 3:
        print("Data shape not supported, should have 3 dimensions. Please try using `CreateRegularGrid()` and then `AddArray()`.")
        return

    extent = [0, s[0] - 1, 0, s[1] - 1, 0, s[2] - 1]
    grid = CreateRegularGrid(extent, name)
    AddPointArray(numpyData, dataName, grid)
    ColorByArray(dataName)

    return grid

# -----------------------------------------------------------------------------
def __GetNumberOfCells(numpyCells):
    """Returns the total number of cells.
    """
    nb = 0
    for cells in numpyCells:
        nb += len(cells)
    return nb

# -----------------------------------------------------------------------------
def CreateUnstructuredGrid(numpyPoints, numpyCells, cellTypes, nbOfCells=None, maxCellSize=8, name="MyGrid"):
    """Create a producer with a vtkUnstructuredGrid. Works only on builtin server.

    numpyPoints (double array with shape (nb_points, 3)): the coords of the nodes.
    numpyCells (list of (nb_cells_of_this_type, nb_nodes_per_this_kind_of_cell) shaped
        uint arrays): one array per cell type, each array contains the nodes defining
        the cells.
    cellTypes (int list): the vtk cell types of the cells (the vtk type of the cells
        in cellList[i] is cellTypes[i]).
    nbOfCells (int, optional): the total number of cells of the unstructured grid.
    maxCellSize (int, optional): the greatest number of nodes defining a cell.
    name (string, optional): the name of the grid in the pipelineBrowser.
    """
    points = vtk.vtkPoints()
    nb_points = len(numpyPoints)
    points.Allocate(nb_points)
    for i in range(nb_points):
        points.InsertPoint(i, numpyPoints[i])

    ugrid = vtk.vtkUnstructuredGrid()
    ugrid.SetPoints(points)

    if nbOfCells is None:
        nbOfCells = __GetNumberOfCells(numpyCells)
    ugrid.AllocateEstimate(nbOfCells, maxCellSize)

    for t in range(len(cellTypes)):
        vtk_type = cellTypes[t]
        for i in range(len(numpyCells[t])):
            ugrid.InsertNextCell(vtk_type, len(numpyCells[t][i]), numpyCells[t][i])

    return __CreatePipelineSource(ugrid, name)

# -----------------------------------------------------------------------------
def RemoveFromPipeline(source=None):
    """ Remove the given source from the pipeline. This source should not have any children.
    """
    if not source:
        source = GetActiveSource()
    if not source:
        raise RuntimeError ("Could not locate source to remove")

    for cc in range(source.SMProxy.GetNumberOfConsumers()):
        consumer = source.SMProxy.GetConsumerProxy(cc)
        # do not remove sources that have children
        if consumer in GetSources().values():
            print('Cannot remove a source that have children.')
            return

    Delete(source)
    del source

# *****************
# Data Array
# *****************

# -----------------------------------------------------------------------------
def AddPointArray(data, name="MyDataArray", tags=None, proxy=None):
    AddArray(data, name, proxy, "POINT", tags=tags)

# -----------------------------------------------------------------------------
def AddCellArray(data, name="MyDataArray", tags=None, proxy=None):
    AddArray(data, name, proxy, "CELL", tags=tags)

# -----------------------------------------------------------------------------
def AddArray(data, name, proxy, association, tags=None):
    """Adds data array. If an array with the same name already exists in data structure,
    then the new one will replace it.

    data (double array with shape (nb_of_values, nb_of_components)): the data associated to
        points or cells.
    name (string): the name of the array.
    association (string): 'POINT' or 'CELL'.
    tags (uint array, optional): the ids of the points or cells that data is associated.
    """
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'AddArray' on")

    cso = proxy.GetClientSideObject()
    grid = cso.GetOutputDataObject(0)
    
    if association == "POINT":
        nbOfTuples = grid.GetNumberOfPoints()
        fieldata = grid.GetPointData()
    elif association == "CELL":
        nbOfTuples = grid.GetNumberOfCells()
        fieldata = grid.GetCellData()
    else:
        raise ValueError("Unsupported association type")

    nbOfComponents = len(data[0])

    if type(data.tolist()[0]) == int:
        VTK_data = vtk.vtkIntArray()
    else:
        VTK_data = vtk.vtkDoubleArray()
    VTK_data.SetName(name)
    VTK_data.SetNumberOfComponents(nbOfComponents)
    VTK_data.SetNumberOfTuples(nbOfTuples)

    if tags is None:
        tags = range(len(data))

    if nbOfComponents == 1:
        fieldata.SetActiveScalars(name)
        for i in range(len(tags)):
            VTK_data.SetTuple1(tags[i], data[i][0])    
    elif nbOfComponents == 2:
        fieldata.SetActiveVectors(name)
        for i in range(len(tags)):
            VTK_data.SetTuple2(tags[i], data[i][0], data[i][1])    
    elif nbOfComponents == 3:
        fieldata.SetActiveVectors(name)
        for i in range(len(tags)):
            VTK_data.SetTuple3(tags[i], data[i][0], data[i][1], data[i][2])    
    elif nbOfComponents == 4:
        fieldata.SetActiveVectors(name)
        for i in range(len(tags)):
            VTK_data.SetTuple4(tags[i], data[i][0], data[i][1], data[i][2], data[i][3])    
    elif nbOfComponents == 6:
        fieldata.SetActiveVectors(name)
        for i in range(len(tags)):
            VTK_data.SetTuple6(tags[i], data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5])    
    elif nbOfComponents == 9:
        fieldata.SetActiveTensors(name)
        for i in range(len(tags)):
            VTK_data.SetTuple9(tags[i], data[i][0], data[i][1], data[i][2], data[i][3], data[i][4], data[i][5], data[i][6], data[i][7], data[i][8])    
    else:
        raise ValueError("Field unsupported by ParaView (bad number of components)")

    fieldata.AddArray(VTK_data)

    proxy.MarkModified(None)
    proxy.UpdatePipeline()

# -----------------------------------------------------------------------------
def RemoveArray(id, proxy=None):
    """ Remove the array corresponding to id.
    id can be either the name or the index of the array.
    """
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'RemoveArray' on")

    grid = proxy.GetClientSideObject().GetOutputDataObject(0)
    grid.GetPointData().RemoveArray(id)

# -----------------------------------------------------------------------------
def ColorByArray(arrayName, proxy=None):
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'AddArray' on")

    rep = Show(proxy)
    ColorBy(rep, arrayName)
    UpdateScalarBars()
    Render()

# *****************
# Selection
# *****************

# -----------------------------------------------------------------------------
def GetSelection(proxy=None, port=0):
    if not proxy:
        proxy = GetActiveSource()
    if not proxy:
        raise RuntimeError ("Could not locate proxy to 'GetSelection' on")

    sel = proxy.GetSelectionInput(port)
    if sel == None:
        return ["", []]

    field = proxy.GetSelectionInput(port).FieldType
    ids = proxy.GetSelectionInput(port).IDs
    # IDs is on format [0, id0, 0, id1, ..., 0, idn]
    ids = [ids[i] for i in range(1, len(ids), 2)]
    return [field, ids]

# -----------------------------------------------------------------------------
def GetSelectedCells(proxy=None, port=0):
    sel = GetSelection(proxy, port)
    return sel[1] if sel[0] == "CELL" else []

# -----------------------------------------------------------------------------
def GetSelectedPoints(proxy=None, port=0):
    sel = GetSelection(proxy, port)
    return sel[1] if sel[0] == "POINT" else []

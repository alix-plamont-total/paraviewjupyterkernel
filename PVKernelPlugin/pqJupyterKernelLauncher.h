#ifndef pqJupyterKernelLauncher_h
#define pqJupyterKernelLauncher_h

#include <QObject>

class pqJupyterKernelLauncherInternal;

class pqJupyterKernelLauncher : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  pqJupyterKernelLauncher(QObject* p = nullptr);
  ~pqJupyterKernelLauncher() = default;

  // Callback for shutdown.
  void onShutdown();

  // Callback for startup.
  void onStartup();

private:
  Q_DISABLE_COPY(pqJupyterKernelLauncher)
  pqJupyterKernelLauncherInternal* Internal;
};

#endif
